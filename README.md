# SmolCStr
A header-only C library for small-string container type.

## Instructions
See `smolcstr_test.c` for examples.

```
$ make
$ ./a.out
$ make clean
```

## Changelog
- 2023-05-06: Init
