.PHONY: test clean

a.out: smolcstr_test.c
	$(CC) -ggdb -Wpedantic -Werror -Wall -Wextra -Wconversion -std=c2x $^

test: smolcstr_test.c
	$(CC) -m32 -ggdb -Wpedantic -Werror -Wall -Wextra -Wconversion -std=c2x $^
	./a.out > /dev/null
	$(CC) -m64 -ggdb -Wpedantic -Werror -Wall -Wextra -Wconversion -std=c2x $^
	valgrind ./a.out > /dev/null
	$(CC) -ggdb -Wpedantic -Werror -Wall -Wextra -Wconversion -std=c2x $^
	valgrind ./a.out > /dev/null

clean: a.out
	$(RM) -- ./a.out
