#ifndef SMOLCSTR_H_
#define SMOLCSTR_H_

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

/// SmolCStr can store a string of length upto 22 bytes (on 64-bit systems) directly on the stack
/// and requires heap allocation for longer strings. This assumes little-endian system and the last
/// byte of the struct SmolCStr is used as an indicator byte. An indicator byte with its MSB set to
/// 1 indicates that the string is small. The rest of the 7 bits of the indicator byte are used for
/// storing the length of the small string.
///
/// Method 1:
/// This method assumes the most-significant byte of the `capacity` as the indicator byte. It
/// requires the system to be little-endian in all cases.
///
/// Method 2:
/// This method assumes the last byte in the struct (including padding bytes) as the indicator byte.
/// It requires the system to be little-endian only in cases where no trailing padding bytes are
/// used in the struct. That's almost always the case if all the three fields of the struct are of
/// register size (char *, size_t, size_t).
/// Method 2 has the advantage of using all the bytes in the struct (including the padding bytes)
/// when the string is small. 1st method can only store a small string upto the `capacity` field.
///
/// In the case where the most-significant byte of the `capacity` ends up being used as an indicator
/// byte, it is assumed that the MSB of `capacity` can never be 1 because that would mean a very
/// very large allocation.
///
/// TODO: account for a big-endian system.
/// DONE: remove the manual struct packing as the padding bytes can be used for small string.
struct SmolCStr {
	char *buf;
	size_t len;
	size_t cap;
}; // __attribute__((packed, aligned(1)));

void smolcstr_default(struct SmolCStr *self) {
	((unsigned char *) self)[sizeof(struct SmolCStr) - 1] = 0x80u;
}

void smolcstr_new(struct SmolCStr *self, const char *str, size_t len) {
	// No need to 0-initialize the struct as all the relevant bits are set manually later.

	// Buffer where the string will be stored. It can point to either stack or heap.
	char *buf = NULL;

	// Only `sizeof(SmolCStr) - 2` bytes can be used for storing the string.
	// The leftover two bytes are used for storing the null terminator and an indicator byte.
	if (len + 2 <= sizeof(struct SmolCStr)) {
		// Store length of the string in the remaining bits of the indicator byte as `len <= 0x7F`.
		((unsigned char *) self)[sizeof(struct SmolCStr) - 1] = 0x80u | (unsigned char) len;
		buf = (char *) self;
	} else {
		self->len = len;
		self->cap = len + 1u;

		// TODO: remove the dependency on malloc.
		self->buf = (char *) malloc(self->cap);
		if (self->buf == NULL) {
			// TODO: handle this case in a better way.
			assert(0 && "ERROR: failed to allocate memory for the string");
		}

		buf = self->buf;
	}

	// NOTE: can use `memcpy` here but this implementation is intended to have very little
	// dependency on the stdlib.
	size_t idx = 0;

	while (idx < len) {
		buf[idx] = str[idx];
		idx += 1;
	}

	buf[idx] = '\0';
}

/// TODO: return the result using a parameter instead of returning from the function.
struct SmolCStr smolcstr_from(const char *str, size_t len) {
	struct SmolCStr smolcstr;
	smolcstr_new(&smolcstr, str, len);
	return smolcstr;
}

static inline bool smolcstr_is_small(const struct SmolCStr *self) {
	assert(self != NULL);

	const unsigned char indicator = ((unsigned char *) self)[sizeof(struct SmolCStr) - 1];
	return indicator & 0x80u;
}

/// This function currently tries to provide a read-only view of the string.
/// The returned string is null-terminated and can be used as a normal C string.
/// TODO: remove the const qualifier from the return type.
const char *smolcstr_as_ptr(const struct SmolCStr *self) {
	assert(self != NULL);

	if (smolcstr_is_small(self)) {
		return (const char *) self;
	} else {
		return (const char *) self->buf;
	}
}

size_t smolcstr_len(const struct SmolCStr *self) {
	assert(self != NULL);

	if (smolcstr_is_small(self)) {
		const unsigned char indicator = ((unsigned char *) self)[sizeof(struct SmolCStr) - 1];
		return indicator & 0x7Fu;
	} else {
		return self->len;
	}
}

size_t smolcstr_capacity(const struct SmolCStr *self) {
	assert(self != NULL);

	if (smolcstr_is_small(self)) {
		return sizeof(struct SmolCStr) - 2;
	} else {
		// `- 1` because the last byte is reserved for the null terminator.
		return self->cap - 1;
	}
}

void smolcstr_free(struct SmolCStr *self) {
	assert(self != NULL);

	if (smolcstr_is_small(self)) {
		return;
	}

	free(self->buf);

	self->buf = NULL;
	self->cap = 0;
	self->len = 0;
}

#endif // SMOLCSTR_H_
