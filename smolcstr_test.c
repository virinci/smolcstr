#include "smolcstr.h"

#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
	typedef struct SmolCStr SmolCStr;
	static_assert(sizeof(SmolCStr) == sizeof(char *) + sizeof(size_t) + sizeof(size_t), "OOPS");

	const char *movie =
		"According to all known laws of aviation, there is no way a bee should be able to fly.";

	{
		static const size_t size = sizeof(SmolCStr);
		SmolCStr ss = smolcstr_from(movie, size - 2);
		assert(smolcstr_is_small(&ss));
		assert(smolcstr_len(&ss) == size - 2);
		assert(smolcstr_capacity(&ss) == size - 2 && smolcstr_capacity(&ss) >= smolcstr_len(&ss));

		printf("`%s`\n", smolcstr_as_ptr(&ss));
		// This results in UB but works for the small string case.
		printf("`%s`\n", (char *) &ss);
		smolcstr_free(&ss);
	}

	{
		SmolCStr ss = smolcstr_from(movie, strlen(movie));
		assert(!smolcstr_is_small(&ss));
		assert(smolcstr_len(&ss) == strlen(movie));
		assert(smolcstr_capacity(&ss) > 22 && smolcstr_capacity(&ss) >= smolcstr_len(&ss));

		const char *ssp = smolcstr_as_ptr(&ss);
		printf("`%s`\n", ssp);
		// This doesn't work because the string is not small.
		printf("`%s`\n", (char *) &ss);
		smolcstr_free(&ss);
	}

	return 0;
}
